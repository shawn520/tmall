package tmall.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tmall.bean.Order;
import tmall.bean.User;
import tmall.util.DBUtil;
import tmall.util.DateUtil;

public class OrderDAO {

	//获取订单总数
	public int getTotal(){
		int total = 0;
		try(Connection c = DBUtil.getConnection();Statement s = c.createStatement();){
			String sql = "select count(*) from order_";
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()){
				total = rs.getInt(1);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return total;
	}
	
	//增加订单信息
	public void add(Order bean){
		String sql = "insert into order_ values(null,?,?,?,?,?,?,?,?,?,?,?,?";
		try(Connection c=DBUtil.getConnection();PreparedStatement ps=c.prepareStatement(sql);){
			ps.setInt(1, bean.getUser().getId());
			ps.setString(2, bean.getOrderCode());
			ps.setString(3, bean.getAddress());
			ps.setString(4, bean.getPost());
			ps.setString(5, bean.getReceiver());
			ps.setString(6, bean.getMobile());
			ps.setString(7, bean.getUserMessage());
			ps.setTimestamp(8, DateUtil.d2t(bean.getCreateDate()));
			ps.setTimestamp(9, DateUtil.d2t(bean.getPayDate()));
			ps.setTimestamp(10, DateUtil.d2t(bean.getDeliveryDate()));
			ps.setTimestamp(11, DateUtil.d2t(bean.getConfirmDate()));
			ps.setString(12, bean.getStatus());
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			if(rs.next()){
				int id = rs.getInt(1);
				bean.setId(id);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	//修改订单信息
	public void update(Order bean){
		String sql="update order_ set uid=?,orderCode=?,address=?,post=?,receiver=?,mobile=?,userMessage=?,createDate=?,payDate=?,deliveryDate=?,confirmDate=?,status=? where id=?";
		try(Connection c= DBUtil.getConnection();PreparedStatement ps=c.prepareStatement(sql);){
			ps.setInt(1, bean.getUser().getId());
			ps.setString(2, bean.getOrderCode());
			ps.setString(3, bean.getAddress());
			ps.setString(4, bean.getPost());
			ps.setString(5, bean.getReceiver());
			ps.setString(6, bean.getMobile());
			ps.setString(7, bean.getUserMessage());
			ps.setTimestamp(8, DateUtil.d2t(bean.getCreateDate()));
			ps.setTimestamp(9, DateUtil.d2t(bean.getPayDate()));
			ps.setTimestamp(10, DateUtil.d2t(bean.getDeliveryDate()));
			ps.setTimestamp(11, DateUtil.d2t(bean.getConfirmDate()));
			ps.setString(12, bean.getStatus());
			ps.setInt(13, bean.getId());
			ps.execute();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	//
	public void delete(int id){
		try(Connection c = DBUtil.getConnection();Statement s = c.createStatement();){
			String sql = "delete from order_ where id="+id;
			s.execute(sql);
		} catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	//根据id获取订单信息
	public Order get(int id){
		Order order = null;
		try(Connection c=DBUtil.getConnection();Statement s=c.createStatement();){
			String sql = "select * from order_ where id="+id;
			ResultSet rs = s.executeQuery(sql);
			if(rs.next()){
				order = new Order();
				int uid = rs.getInt("uid");
				String orderCode=rs.getString("orderCode");
				String address = rs.getString("address");
				String post = rs.getString("post");
				String receiver = rs.getString("receiver");
				String mobile = rs.getString("mobile");
				String userMessage = rs.getString("userMessage");
				Date createDate = DateUtil.t2d(rs.getTimestamp("createDate"));
				Date payDate = DateUtil.t2d(rs.getTimestamp("payDate"));
				Date deliveryDate = DateUtil.t2d(rs.getTimestamp("deliveryDate"));
				Date confirmDate = DateUtil.t2d(rs.getTimestamp("confirmDate"));
				String status = rs.getString("status");
				
				User user = new UserDAO().get(uid);
				
				order.setId(id);
				order.setUser(user);
				order.setOrderCode(orderCode);
				order.setAddress(address);
				order.setPost(post);
				order.setReceiver(receiver);
				order.setMobile(mobile);
				order.setUserMessage(userMessage);
				order.setCreateDate(createDate);
				order.setPayDate(payDate);
				order.setDeliveryDate(deliveryDate);
				order.setConfirmDate(confirmDate);
				order.setStatus(status);
				
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return order;
	}
	
	public List<Order> list(){
		return list(0,Short.MAX_VALUE);
	}
	public List<Order> list(int start,int count){
		List<Order> beans= new ArrayList<>();
		String sql = "select * from order_ order by id desc limit ?,?";
		try(Connection c= DBUtil.getConnection();PreparedStatement ps = c.prepareStatement(sql);){
			ps.setInt(1, start);
			ps.setInt(2, count);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Order bean = new Order();
				int id =rs.getInt("id");
				int uid = rs.getInt("uid");
				String orderCode = rs.getString("orderCode");
				String address = rs.getString("address");
				String post = rs.getString("post");
				String receiver = rs.getString("receiver");
				String mobile =rs.getString("mobile");
				String userMessage = rs.getString("userMessage");
				Date createDate = DateUtil.t2d(rs.getTimestamp("createDate"));
				Date payDate = DateUtil.t2d(rs.getTimestamp("payDate"));
				Date deliveryDate = DateUtil.t2d(rs.getTimestamp("deliveryDate"));
				Date confirmDate = DateUtil.t2d(rs.getTimestamp("confirmDate"));
				String status = rs.getString("status");
				
				User user = new UserDAO().get(uid);
				bean.setId(id);
				bean.setUser(user);
				bean.setOrderCode(orderCode);
				bean.setAddress(address);
				bean.setPost(post);
				bean.setReceiver(receiver);
				bean.setMobile(mobile);
				bean.setUserMessage(userMessage);
				bean.setCreateDate(createDate);
				bean.setPayDate(payDate);
				bean.setDeliveryDate(deliveryDate);
				bean.setConfirmDate(confirmDate);
				bean.setStatus(status);
				beans.add(bean);
				
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return beans;
	}
	
	public List<Order> list(int uid,String excludedStatus){
		return list(uid,excludedStatus,0,Short.MAX_VALUE);
	}
	public List<Order> list(int uid,String excludedStatus,int start,int count){
		List<Order> beans = new ArrayList<>();
		String sql = "select * from order_ where uid = ? and status != excudedStatus order by id desc limit ?,?";
		try(Connection c = DBUtil.getConnection();PreparedStatement ps = c.prepareStatement(sql);){
			ps.setInt(1, uid);
			ps.setString(2, excludedStatus);
			ps.setInt(3, start);
			ps.setInt(4, count);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Order bean = new Order();
				int id = rs.getInt(1);
				String orderCode = rs.getString("orderCode");
				String address = rs.getString("address");
				String post = rs.getString("post");
				String receiver = rs.getString("recerver");
				String mobile = rs.getString("mobile");
				String userMessage = rs.getString("userMessage");
				Date createDate  = DateUtil.t2d(rs.getTimestamp("createDate"));
				Date payDate = DateUtil.t2d(rs.getTimestamp("payDate"));
				Date deliveryDate = DateUtil.t2d(rs.getTimestamp("deliveryDate"));
				Date confirmDate = DateUtil.t2d(rs.getTimestamp("confirmDate"));
				String status = rs.getString("status");
				
				User user = new UserDAO().get(uid);
				bean.setId(id);
				bean.setUser(user);
				bean.setOrderCode(orderCode);
				bean.setAddress(address);
				bean.setPost(post);
				bean.setReceiver(receiver);
				bean.setMobile(mobile);
				bean.setUserMessage(userMessage);
				bean.setCreateDate(createDate);
				bean.setPayDate(payDate);
				bean.setDeliveryDate(deliveryDate);
				bean.setConfirmDate(confirmDate);
				bean.setStatus(status);
				beans.add(bean);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return beans;
	}
	
}
