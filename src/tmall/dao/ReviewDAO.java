package tmall.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tmall.bean.Product;
import tmall.bean.Review;
import tmall.bean.User;
import tmall.util.DBUtil;
import tmall.util.DateUtil;

public class ReviewDAO {

	//获取所有评论数
	public int getTotal(){
		int total = 0;
		try(Connection c =DBUtil.getConnection();Statement s = c.createStatement();){
			String sql = "select count(*) from review";
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()){
				total = rs.getInt(1);
			}
		} catch(SQLException e){
			e.printStackTrace();
		}
		return total;
	}
	
	//获取某个产品的评论数
	public int getTotal(int pid){
		int total = 0;
		try(Connection c =DBUtil.getConnection();Statement s = c.createStatement();){
			String sql = "select count(*) from review where pid = "+ pid;
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()){
				total = rs.getInt(1);
			}
		} catch(SQLException e){
			e.printStackTrace();
		}
		return total;
	}
	
	//增加一条评论
	public void add(Review bean){
		String sql = "insert into review values(null,?,?,?,?)";
		try(Connection c = DBUtil.getConnection(); PreparedStatement ps = c.prepareStatement(sql);){
			ps.setString(1, bean.getContent());
			ps.setTimestamp(2, DateUtil.d2t(bean.getCreateDate()));
			Product product = bean.getProduct();
			ps.setInt(3, product.getId());
			User user = bean.getUser();
			ps.setInt(4, user.getId());
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			if(rs.next()){
				int id = rs.getInt(1);
				bean.setId(id);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	//更新一条评论
	public void update(Review bean){
		String sql = "update review set content=?,createDate=?,pid=?,uid=? where id=?";
		try(Connection c= DBUtil.getConnection();PreparedStatement ps = c.prepareStatement(sql);){
			
			ps.setString(1, bean.getContent());
			ps.setTimestamp(2, DateUtil.d2t(bean.getCreateDate()));
			ps.setInt(3, bean.getProduct().getId());
			ps.setInt(4, bean.getUser().getId());
			ps.setInt(5, bean.getId());
			ps.execute();
		} catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	//删除评论
	public void delete(int id){
		try(Connection c = DBUtil.getConnection();Statement s = c.createStatement();){
			String sql = "delete from review where id =" + id;
			s.execute(sql);
		} catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public Review get(int id){
		Review bean = null;
		try(Connection c = DBUtil.getConnection();Statement s = c.createStatement();){
			String sql = "select * from review where id="+id;
			ResultSet rs = s.executeQuery(sql);
			if(rs.next()){
				bean = new Review();
				String content = rs.getString("content");
				Date createDate = DateUtil.t2d(rs.getTimestamp("createDate"));
				int pid = rs.getInt("pid");
				int uid = rs.getInt("uid");
				bean.setId(id);
				bean.setContent(content);
				bean.setCreateDate(createDate);
				Product product = new ProductDAO().get(pid);
				bean.setProduct(product);
				User user = new UserDAO().get(uid);
				bean.setUser(user);
			}
		} catch(SQLException e){
			e.printStackTrace();
		}
		return bean;
	}
	
	//获取指定产品的所有评价
	public List<Review> list(int pid){
		return list(pid,0,Short.MAX_VALUE);
	}
	
	//分页
	public List<Review> list(int pid,int start,int count){
		List<Review> beans = new ArrayList<>();
		String sql = "select * from review where pid= ? order by id desc limit ?,?";
		try(Connection c = DBUtil.getConnection();PreparedStatement ps = c.prepareStatement(sql);){
			ps.setInt(1, pid);
			ps.setInt(2, start);
			ps.setInt(3, count);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Review bean = new Review();
				int id = rs.getInt(1);
				String content = rs.getString("content");
				Date createDate = DateUtil.t2d(rs.getTimestamp("createDate"));
				int uid = rs.getInt("uid");
				
				Product product = new ProductDAO().get(pid);
				User user = new UserDAO().get(uid);
				
				bean.setId(id);
				bean.setContent(content);
				bean.setCreateDate(createDate);
				bean.setProduct(product);
				bean.setUser(user);
				beans.add(bean);
			}
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return beans;
	}
	
	//查看评论是否存在，个人感觉这个功能没啥用处
	public boolean isExist(String content, int pid){
		String sql = "select * from review where content=? and id = ?";
		try(Connection c = DBUtil.getConnection();PreparedStatement ps = c.prepareStatement(sql);){
			ps.setString(1, content);
			ps.setInt(2, pid);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				return true;
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return false;
	}
	
	
}
