package tmall.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import tmall.bean.Order;
import tmall.bean.OrderItem;
import tmall.bean.Product;
import tmall.bean.User;
import tmall.util.DBUtil;

public class OrderItemDAO {

	public int getTotal(){
		int total = 0;
		try(Connection c= DBUtil.getConnection();Statement s= c.createStatement();){
			String sql ="select count(*) from orderitem";
			ResultSet rs=s.executeQuery(sql);
			while(rs.next()){
				total=rs.getInt(1);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return total;
	}
	
	public void add(OrderItem bean){


		String sql = "insert into orderitem values(null,?,?,?,?)";
		try(Connection c=DBUtil.getConnection();PreparedStatement ps=c.prepareStatement(sql);){
			ps.setInt(1, bean.getOrder().getId());
			ps.setInt(2, bean.getProduct().getId());
			ps.setInt(3, bean.getUser().getId());
			ps.setInt(4, bean.getNumber());
			ps.execute();
			ResultSet rs=ps.getGeneratedKeys();
			if(rs.next()){
				int id = rs.getInt(1);
				bean.setId(id);	
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void update(OrderItem bean){
		String sql ="update orderitem set oid=?,pid=?,uid=?,num=? where id=?";
		try(Connection c=DBUtil.getConnection();PreparedStatement ps=c.prepareStatement(sql);){
			ps.setInt(1, bean.getOrder().getId());
			ps.setInt(2, bean.getProduct().getId());
			ps.setInt(3, bean.getUser().getId());
			ps.setInt(4, bean.getNumber());
			ps.setInt(5, bean.getId());
			ps.execute();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void delete(int id){
		try(Connection c=DBUtil.getConnection();Statement s= c.createStatement();){
			String sql ="delete from orderitem where id="+id;
			s.execute(sql);
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public OrderItem get(int id){
		OrderItem bean = null;
		try(Connection c=DBUtil.getConnection();Statement s= c.createStatement();){
			String sql = "select * from orderitem where id ="+id;
			ResultSet rs=s.executeQuery(sql);
			if(rs.next()){
				bean = new OrderItem();
				int oid =rs.getInt("oid");
				int pid = rs.getInt("pid");
				int uid =rs.getInt("uid");
				int num =rs.getInt("num");
				
				Order order = new OrderDAO().get(oid);
				Product product = new ProductDAO().get(pid);
				User user = new UserDAO().get(uid);

				bean.setId(id);
				bean.setNum(num);
				bean.setOrder(order);
				bean.setProduct(product);
				bean.setUser(user);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return bean;
	}
	
	//显示用户的所有订单项
	public List<OrderItem> listByUser(int uid){
		return listByUser(uid,0,Short.MAX_VALUE);
	}
	
	//显示指定用户的分页订单项
	public List<OrderItem> listByUser(int uid, int start,int count){
		List<OrderItem> beans = new ArrayList<>();
		String sql = "select * from orderitem where uid=? order by id desc,limit ?,?";
		try(Connection c = DBUtil.getConnection();PreparedStatement ps=c.prepareStatement(sql);){
			ps.setInt(1, uid);
			ps.setInt(2, start);
			ps.setInt(3, count);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				OrderItem bean =new OrderItem();
				int id = rs.getInt(1);
				int oid = rs.getInt("oid");
				int pid = rs.getInt("pid");
				int num = rs.getInt("num");
				
				Order order = new OrderDAO().get(oid);
				Product product = new ProductDAO().get(pid);
				
				bean.setId(id);
				bean.setOrder(order);
				bean.setProduct(product);
				bean.setNum(num);
				beans.add(bean);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return beans;
	}
	
	public List<OrderItem> listByOrder(int oid){
		return listByOrder(oid,0,Short.MAX_VALUE);
	}
	
	//根据订单号来显示订单项
	public List<OrderItem> listByOrder(int oid,int start,int count){
		List<OrderItem> beans = new ArrayList<>();
		String sql = "select * from orderitem where oid=? order by id desc,limit ?,?";
		try(Connection c = DBUtil.getConnection();PreparedStatement ps=c.prepareStatement(sql);){
			ps.setInt(1, oid);
			ps.setInt(2, start);
			ps.setInt(3, count);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				OrderItem bean =new OrderItem();
				int id = rs.getInt(1);
				int uid = rs.getInt("uid");
				int pid = rs.getInt("pid");
				int num = rs.getInt("num");
				
				Order order = new OrderDAO().get(oid);
				Product product = new ProductDAO().get(pid);
				
				bean.setId(id);
				bean.setOrder(order);
				bean.setProduct(product);
				bean.setNum(num);
				beans.add(bean);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return beans;
	}
	
	public List<OrderItem> listByProduct(int pid){
		return listByProduct(pid,0,Short.MAX_VALUE);
	}
	
	public List<OrderItem> listByProduct(int pid,int start,int count){
		List<OrderItem> beans = new ArrayList<>();
		String sql = "select * from orderitem where oid=? order by id desc,limit ?,?";
		try(Connection c = DBUtil.getConnection();PreparedStatement ps=c.prepareStatement(sql);){
			ps.setInt(1, pid);
			ps.setInt(2, start);
			ps.setInt(3, count);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				OrderItem bean =new OrderItem();
				int id = rs.getInt(1);
				int uid = rs.getInt("uid");
				int oid = rs.getInt("oid");
				int num = rs.getInt("num");
				
				Order order = new OrderDAO().get(oid);
				Product product = new ProductDAO().get(pid);
				
				bean.setId(id);
				bean.setOrder(order);
				bean.setProduct(product);
				bean.setNum(num);
				beans.add(bean);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return beans;
	}
	
	//为订单设置订单项集合，这个不太懂。
	public void fill(List<Order> os){
		for(Order o:os){
			List<OrderItem> ois = listByOrder(o.getId());
			float total = 0;
			int totalNumber = 0;
			for(OrderItem oi : ois) {
				total += oi.getNumber()*oi.getProduct().getPromotePrice();
				totalNumber += oi.getNumber();
			}
			o.setTotal(total);
			o.setOrderItem(ois);
			o.setTotalNumber(totalNumber);			
		}
	}
	
	 public void fill(Order o){
	        List<OrderItem> ois=listByOrder(o.getId());
	        float total = 0;
	        for (OrderItem oi : ois) {
	             total+=oi.getNumber()*oi.getProduct().getPromotePrice();
	        }
	        o.setTotal(total);
	        o.setOrderItem(ois);
	    }

	

}


