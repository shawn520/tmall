package tmall.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tmall.bean.Category;
import tmall.bean.Product;
import tmall.bean.Property;
import tmall.util.DBUtil;
import tmall.util.DateUtil;

public class ProductDAO {

	//获取分类下的产品数目
	public int getTotal(int cid){
		int total = 0;
		try(Connection c = DBUtil.getConnection();Statement s = c.createStatement();){
			String sql = "select count(*) from product where cid ="+cid;
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()){
				total = rs.getInt(1);
			}
		} catch(SQLException e){
			e.printStackTrace();
		}
		return total;
	}
	
	//增加一个产品
	public void add(Product bean){
		String sql = "insert into product values(null,?,?,?,?,?,?)";
		try(Connection c = DBUtil.getConnection();PreparedStatement ps=c.prepareStatement(sql);){
			ps.setString(1, bean.getName());
			ps.setString(2, bean.getSubTitle());
			ps.setFloat(3, bean.getOriginalPrice());
			ps.setFloat(4, bean.getPromotePrice());
			ps.setInt(5, bean.getStock());
			ps.setInt(6, bean.getCategory().getId());
			ps.setTimestamp(7, DateUtil.d2t(bean.getCreateDate()));
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			if(rs.next()){
				int id = rs.getInt(1);
				bean.setId(id);
			}	
		} catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	//修改产品信息
	public void update(Product bean){
		String sql="update product set name=?,subTitle=?,originalPrice=?,promotePrice=?,stock=?,cid=?,createDate=?";
		try(Connection c= DBUtil.getConnection();PreparedStatement ps = c.prepareStatement(sql)){
			ps.setString(1, bean.getName());
			ps.setString(2, bean.getSubTitle());
			ps.setFloat(3, bean.getOriginalPrice());
			ps.setFloat(4, bean.getPromotePrice());
			ps.setInt(5, bean.getStock());
			ps.setInt(6, bean.getCategory().getId());
			ps.setTimestamp(7, DateUtil.d2t(bean.getCreateDate()));
			ps.execute(sql);
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	public void delete(int id){
		try(Connection c = DBUtil.getConnection();Statement s = c.createStatement();){
			String sql = "delete from product where id =" +id;
			s.execute(sql);
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	//根据id获取产品
	public Product get(int id){
		Product bean = null;
		try(Connection c = DBUtil.getConnection();Statement s = c.createStatement();){
			String sql = "select * from product where id ="+id;
			ResultSet rs = s.executeQuery(sql);
			if(rs.next()){
				bean = new Product();
				String name = rs.getString("name");
				String subTitle=rs.getString("subTitle");
				float originalPrice = rs.getFloat("originalPrice");
				float promotePrice = rs.getFloat("promotePrice");
				int stock =rs.getInt("stock");
				Date createDate = DateUtil.d2t(rs.getTimestamp("createDate"));
				int cid = rs.getInt("cid");
				Category category = new CategoryDAO().get(cid);//这句自己没有想出来
				bean.setId(id);
				bean.setName(name);
				bean.setSubTitle(subTitle);
				bean.setOriginalPrice(originalPrice);
				bean.setPromotePrice(promotePrice);
				bean.setStock(stock);
				bean.setCreateDate(createDate);
				bean.setCategory(category);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return bean;
	}
	
	//查看分类下的所有产品
	public List<Product> list(int cid){
		return list(cid,0,Short.MAX_VALUE);
	}
	
	//分页，查看分类下的产品
	public List<Product> list(int cid, int start,int count){
		List<Product> beans = new ArrayList<>();
		String sql="select * from product where cid = ? order by id desc,limit ?,?";
		try(Connection c = DBUtil.getConnection();PreparedStatement ps = c.prepareStatement(sql);){
			ps.setInt(1, cid);
			ps.setInt(2, start);
			ps.setInt(3, count);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Product bean = new Product();
				int id = rs.getInt(1);
				String name = rs.getString("name");
				String subTitle=rs.getString("subTitle");
				float originalPrice = rs.getFloat("originalPrice");
				float promotePrice = rs.getFloat("promotePrice");
				int stock =rs.getInt("stock");
				Date createDate = DateUtil.d2t(rs.getTimestamp("createDate"));
				Category category = new CategoryDAO().get(cid);//这句自己没有想出来
				bean.setId(id);
				bean.setName(name);
				bean.setSubTitle(subTitle);
				bean.setOriginalPrice(originalPrice);
				bean.setPromotePrice(promotePrice);
				bean.setStock(stock);
				bean.setCreateDate(createDate);
				bean.setCategory(category);	
				beans.add(bean);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return beans;
	}
	
	//查看所有产品
	public List<Product> list(){
		return list(0,Short.MAX_VALUE);
	}
	
	//分页，查看产品
	public List<Product> list(int start,int count){
		List<Product> beans = new ArrayList<>();
		String sql="select * from product order by id desc,limit ?,?";
		try(Connection c = DBUtil.getConnection();PreparedStatement ps = c.prepareStatement(sql);){
			ps.setInt(1, start);
			ps.setInt(2, count);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Product bean = new Product();
				int id = rs.getInt(1);
				int cid = rs.getInt("cid");
				String name = rs.getString("name");
				String subTitle=rs.getString("subTitle");
				float originalPrice = rs.getFloat("originalPrice");
				float promotePrice = rs.getFloat("promotePrice");
				int stock =rs.getInt("stock");
				Date createDate = DateUtil.d2t(rs.getTimestamp("createDate"));
				Category category = new CategoryDAO().get(cid);//这句自己没有想出来
				bean.setId(id);
				bean.setName(name);
				bean.setSubTitle(subTitle);
				bean.setOriginalPrice(originalPrice);
				bean.setPromotePrice(promotePrice);
				bean.setStock(stock);
				bean.setCreateDate(createDate);
				bean.setCategory(category);	
				beans.add(bean);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return beans;
	}
	
	//为分类填充产品集合，这个地方会头要多理解一下。
	public void fill(List<Category> cs){
		for(Category c : cs)
			fill(c);
	}
	
	public void fill(Category c){
		List<Product> ps = this.list(c.getId());//调用本类list(int cid)方法，返回当前分类下的所有产品赋给ps
		c.setProducts(ps);
	}
	
	//对分类下的产品按行填充
	public void fillByRow(List<Category> cs) {
        int productNumberEachRow = 8;	//声明每行填充个数
        for (Category c : cs) {			//所有分类放在cs，遍历所有分类
            List<Product> products =  c.getProducts();	//将每个分类下的所有产品放入products
            List<List<Product>> productsByRow =  new ArrayList<>();	//声明行list
            for (int i = 0; i < products.size(); i+=productNumberEachRow) {
                int size = i+productNumberEachRow;		//每填充一行size增8
                size= size>products.size()?products.size():size;//确定每行的大小
                List<Product> productsOfEachRow =products.subList(i, size); //每8个产品加入productsOfEachRow
                productsByRow.add(productsOfEachRow);	//将每行的listproductsByRow
            }
            c.setProductsByRow(productsByRow);			//将productsByRow赋值给分类c
        }
    }
	
	public void setFirstProductImage(Product p){
		
	}
	
	
	
}
